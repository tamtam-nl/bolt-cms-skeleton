<?php

namespace Bolt\Extension\TamTam\JsonEndpoints;

use Bolt\Extension\SimpleExtension;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * JsonEndpoints extension class.
 */
class JsonEndpointsExtension extends SimpleExtension
{
    /**
     * {@inheritdoc}
     */
    protected function registerBackendRoutes(ControllerCollection $collection)
    {
        // GET requests on the /bolt/koala route
        $collection->get('/json', [$this, 'callbackJsonGet']);
    }
    
    /**
     * @param Application $app
     * @param Request     $request
     */
    public function callbackJsonGet(Application $app, Request $request)
    {
        return new Response('I am a little teapot', 418);
    }
}
